import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';

import { Hero } from '../../interfaces/hero';

import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({ providedIn: 'root' })
export class HeroService {

  private heroesUrl = 'http://localhost:8080/heroes';

  httpOptions = {
    headers: new HttpHeaders().set('Content-Type', 'application/json')
  };

  constructor(private http: HttpClient) { }
  

  /** GET heroes from the server */
  getHeroes(): Observable<Hero[]> {
    return this.http.get<Hero[]>(`${this.heroesUrl}/allHeroes`);
  }

  /** GET hero by id. Will 404 if id not found */
  getHero(id: number): Observable<Hero> {
    return this.http.get<Hero>(`${this.heroesUrl}/${id}`);
  }

  /** PUT: update the hero on the server */
  updateHero(id: number, hero: Hero): Observable<any> {
    return this.http.put(`${this.heroesUrl}/${id}`, hero, this.httpOptions);
  }

  /** POST: add a new hero to the server */
  addHero(name: string): Observable<Hero> {
    return this.http.post<Hero>(`${this.heroesUrl}/newHero`, name);
  }

  /** DELETE: delete the hero from the server */
  deleteHero(id: number): Observable<Hero> {
    return this.http.delete<Hero>(`${this.heroesUrl}/${id}`);
  }

  /* GET heroes whose name contains search term */
  searchHeroes(term: string): Observable<Hero[]> {
    if (!term.trim()) {
      // if not search term, return empty hero array.
      return of([]);
    }
    return this.http.get<Hero[]>(`${this.heroesUrl}/heroByTerm/${term}`);
  }
}